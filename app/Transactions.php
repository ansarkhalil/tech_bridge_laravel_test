<?php

namespace App;

use Illuminate\Support\Collection;
use Rap2hpoutre\FastExcel\FastExcel;

/**
 * @SWG\Swagger(
 *     basePath="/api/",
 *     produces={"application/json"},
 *     consumes={"application/x-www-form-urlencoded"},
 *     @SWG\Info(version="1.0", title="Tech Bridge Laravel Test API v1"),
 * )
 */
class Transactions extends FastExcel
{
    protected $fastExcel;
    protected $fileName;

    /**
     * Transactions constructor
     *
     * @param File $fileName
     */
    public function __construct($fileName = null)
    {
        $this->fileName = $fileName;
    }

    /**
     * Display all records.
     *
     * @return Collection
     */
    public function showAll()
    {
        $collection = $this->import($this->fileName);
        return $collection;
    }

    /**
     * Save new records.
     *
     * @param  Collection
     */
    public function save(Collection $collection)
    {
        $collection = (new FastExcel($collection))->export($this->fileName);
        return $collection;
    }
}
