<?php


/**
 * @SWG\Get(path="/v1/transactions",
 *     tags={"Transactions"},
 *     summary="Get All ransactions",
 *     @SWG\Response(
 *         response = 200,
 *         description = "Get Collection of Transaction",
 *         examples={
 *     "application/json": {
 *     "data":{
 *          {
 *            "start_date": "02/05/2018",
 *            "end_date": "11/05/2018",
 *           "first_name": "Roberto",
 *           "last_name": "Carron Martin",
 *           "email": "insurance@doyouinsurance.com",
 *           "telnumber": "34964830905",
 *           "address1": "c\\Ronda Mijares n190 Bis",
 *           "Address2": "",
 *           "city": "Castellon",
 *           "country": "ES",
 *           "postcode": "12002",
 *           "product_name": "DYSIH_DE_PLATINUM_8-14",
 *           "cost": "46.29",
 *           "currency": "usd",
 *           "transaction_date": "01/05/2018",
 *
 *     }},"statusCode":true
 *     }
 *   }
 *     ), @SWG\Response(
 *         response = 422,
 *         description = "No Record Found.",
 *         examples={
 *     "application/json": {
 *     "errors":{
 *     {
 *       "field"="message",
 *       "message"="No Record Found"
 *     }
 *     },"statusCode":false
 *   }
 *     }
 *     ),
 * )
 */

/**
 * @SWG\Get(path="/v1/transactions/{telnumber}",
 *     tags={"Transactions"},
 *        @SWG\Parameter(
 *         name="telnumber",
 *         in="path",
 *         type="integer",
 *         description="Telephone Number  ",
 *       required=true,
 *      ),
 *     summary="Get Transaction By TelNumber",
 *     @SWG\Response(
 *         response = 200,
 *         description = "Get Collection of Transaction",
 *         examples={
 *     "application/json": {
 *     "data":{
 *          {
 *            "start_date": "02/05/2018",
 *            "end_date": "11/05/2018",
 *           "first_name": "Roberto",
 *           "last_name": "Carron Martin",
 *           "email": "insurance@doyouinsurance.com",
 *           "telnumber": "34964830905",
 *           "address1": "c\\Ronda Mijares n190 Bis",
 *           "Address2": "",
 *           "city": "Castellon",
 *           "country": "ES",
 *           "postcode": "12002",
 *           "product_name": "DYSIH_DE_PLATINUM_8-14",
 *           "cost": "46.29",
 *           "currency": "usd",
 *           "transaction_date": "01/05/2018",
 *
 *     }},"statusCode":true
 *     }
 *   }
 *     ), @SWG\Response(
 *         response = 422,
 *         description = "No Record Found.",
 *         examples={
 *     "application/json": {
 *     "errors":{
 *     {
 *       "field"="message",
 *       "message"="No Record Found"
 *     }
 *     },"statusCode":false
 *   }
 *     }
 *     ),
 * )
 */

/**
 * @SWG\Post(path="/v1/transactions",
 *     tags={"Transactions"},
 *     @SWG\Parameter(
 *         name="start_date",
 *         in="formData",
 *         type="string",
 *         description="start_date",
 *         required=true,
 *     ),
 *     @SWG\Parameter(
 *         name="end_date",
 *         in="formData",
 *         type="string",
 *         description="end_date",
 *         required=true,
 *     ),
 *     @SWG\Parameter(
 *         name="first_name",
 *         in="formData",
 *         type="string",
 *         description="first_name",
 *         required=true,
 *     ),
 *      @SWG\Parameter(
 *         name="last_name",
 *         in="formData",
 *         type="string",
 *         description="last_name",
 *         required=true,
 *     ),
 *
 *      @SWG\Parameter(
 *         name="email",
 *         in="formData",
 *         type="string",
 *         description="email",
 *         required=true,
 *     ),
 *      @SWG\Parameter(
 *         name="telnumber",
 *         in="formData",
 *         type="string",
 *         description="telnumber",
 *         required=true,
 *     ),
 *      @SWG\Parameter(
 *         name="address1",
 *         in="formData",
 *         type="string",
 *         description="address1",
 *         required=true,
 *     ),
 *      @SWG\Parameter(
 *         name="Address2",
 *         in="formData",
 *         type="string",
 *         description="Address2",
 *         required=true,
 *     ),
 *      @SWG\Parameter(
 *         name="city",
 *         in="formData",
 *         type="string",
 *         description="city",
 *         required=true,
 *     ),
 *      @SWG\Parameter(
 *         name="country",
 *         in="formData",
 *         type="string",
 *         description="country",
 *         required=true,
 *     ),
 *      @SWG\Parameter(
 *         name="postcode",
 *         in="formData",
 *         type="string",
 *         description="postcode",
 *         required=true,
 *     ),
 *      @SWG\Parameter(
 *         name="product_name",
 *         in="formData",
 *         type="string",
 *         description="product_name",
 *         required=true,
 *     ),
 *      @SWG\Parameter(
 *         name="cost",
 *         in="formData",
 *         type="string",
 *         description="cost",
 *         required=true,
 *     ),
 *      @SWG\Parameter(
 *         name="currency",
 *         in="formData",
 *         type="string",
 *         description="currency",
 *         required=true,
 *     ),
 *      @SWG\Parameter(
 *         name="transaction_date",
 *         in="formData",
 *         type="string",
 *         description="transaction_date",
 *         required=true,
 *     ),
 *
 *     summary="Add Transaction.",
 *    @SWG\Response(
 *         response = 200,
 *         description = "Rider Wallet is Successfully Retrieved",
 *         examples={
 *     "application/json": {
 *       "data":{
 *       "message": "Row Added Successfully"
 *
 *     },"statusCode":true
 *
 *     }
 *   }
 *     ), @SWG\Response(
 *         response = 422,
 *         description = "Invalid email.",
 *         examples={
 *     "application/json": {
 *     "errors":{
 *     {
 *       "field"="email",
 *       "message"="missing or incorrect email"
 *     },
 *     {
 *       "field"="email",
 *       "message"="missing or incorrect email"
 *     }
 *     },"statusCode":false
 *   }
 *     }
 *     ),
 * )
 */

/**
 * @SWG\Put(path="/v1/transactions/{telnumber}",
 *     tags={"Transactions"},
 *     @SWG\Parameter(
 *         name="start_date",
 *         in="formData",
 *         type="string",
 *         description="start_date",
 *         required=true,
 *     ),
 *     @SWG\Parameter(
 *         name="end_date",
 *         in="formData",
 *         type="string",
 *         description="end_date",
 *         required=true,
 *     ),
 *     @SWG\Parameter(
 *         name="first_name",
 *         in="formData",
 *         type="string",
 *         description="first_name",
 *         required=true,
 *     ),
 *      @SWG\Parameter(
 *         name="last_name",
 *         in="formData",
 *         type="string",
 *         description="last_name",
 *         required=true,
 *     ),
 *
 *      @SWG\Parameter(
 *         name="email",
 *         in="formData",
 *         type="string",
 *         description="email",
 *         required=true,
 *     ),
 *      @SWG\Parameter(
 *         name="address1",
 *         in="formData",
 *         type="string",
 *         description="address1",
 *         required=true,
 *     ),
 *      @SWG\Parameter(
 *         name="Address2",
 *         in="formData",
 *         type="string",
 *         description="Address2",
 *         required=true,
 *     ),
 *      @SWG\Parameter(
 *         name="city",
 *         in="formData",
 *         type="string",
 *         description="city",
 *         required=true,
 *     ),
 *      @SWG\Parameter(
 *         name="country",
 *         in="formData",
 *         type="string",
 *         description="country",
 *         required=true,
 *     ),
 *      @SWG\Parameter(
 *         name="postcode",
 *         in="formData",
 *         type="string",
 *         description="postcode",
 *         required=true,
 *     ),
 *      @SWG\Parameter(
 *         name="product_name",
 *         in="formData",
 *         type="string",
 *         description="product_name",
 *         required=true,
 *     ),
 *      @SWG\Parameter(
 *         name="cost",
 *         in="formData",
 *         type="string",
 *         description="cost",
 *         required=true,
 *     ),
 *      @SWG\Parameter(
 *         name="currency",
 *         in="formData",
 *         type="string",
 *         description="currency",
 *         required=true,
 *     ),
 *      @SWG\Parameter(
 *         name="transaction_date",
 *         in="formData",
 *         type="string",
 *         description="transaction_date",
 *         required=true,
 *     ),
 *
 *     summary="Update Transaction.",
 *    @SWG\Response(
 *         response = 200,
 *         description = "Rider Wallet is Successfully Retrieved",
 *         examples={
 *     "application/json": {
 *       "data":{
 *       "message": "Row Added Successfully"
 *
 *     },"statusCode":true
 *
 *     }
 *   }
 *     ), @SWG\Response(
 *         response = 422,
 *         description = "Invalid email.",
 *         examples={
 *     "application/json": {
 *     "errors":{
 *     {
 *       "field"="email",
 *       "message"="missing or incorrect email"
 *     },
 *     {
 *       "field"="email",
 *       "message"="missing or incorrect email"
 *     }
 *     },"statusCode":false
 *   }
 *     }
 *     ),
 * )
 */
/**
 * @SWG\Delete(path="/v1/transactions/{telnumber}",
 *     tags={"Transactions"},
 *     *   @SWG\Parameter(
 *         name="telnumber",
 *         in="path",
 *         type="integer",
 *         description="Telephone Number  ",
 *       required=true,
 *      ),
 *     summary="Delete Transaction By TelNumber",
 *     @SWG\Response(
 *         response = 200,
 *         description = "Get Collection of Transaction",
 *         examples={
 *     "application/json": {
 *     "data":{
 *      "message":"Row Updated Successfully"
 *     },"statusCode":true
 *     }
 *   }
 *     ), @SWG\Response(
 *         response = 422,
 *         description = "No Record Found.",
 *         examples={
 *     "application/json": {
 *     "errors":{
 *     {
 *       "field"="message",
 *       "message"="No Record Found"
 *     }
 *     },"statusCode":false
 *   }
 *     }
 *     ),
 * )
 */

/**
 * @SWG\Get(path="/v1/transactions-search",
 *     tags={"Transactions"},
 *     @SWG\Parameter(
 *         name="field",
 *         in="query",
 *         type="string",
 *         description="Any field",
 *       required=true,
 *      ),
 *     @SWG\Parameter(
 *         name="value",
 *         in="query",
 *         type="string",
 *         description=" field value",
 *       required=true,
 *      ),
 *     summary="Get All ransactions",
 *     @SWG\Response(
 *         response = 200,
 *         description = "Get Collection of Transaction",
 *         examples={
 *     "application/json": {
 *     "data":{
 *          {
 *            "start_date": "02/05/2018",
 *            "end_date": "11/05/2018",
 *           "first_name": "Roberto",
 *           "last_name": "Carron Martin",
 *           "email": "insurance@doyouinsurance.com",
 *           "telnumber": "34964830905",
 *           "address1": "c\\Ronda Mijares n190 Bis",
 *           "Address2": "",
 *           "city": "Castellon",
 *           "country": "ES",
 *           "postcode": "12002",
 *           "product_name": "DYSIH_DE_PLATINUM_8-14",
 *           "cost": "46.29",
 *           "currency": "usd",
 *           "transaction_date": "01/05/2018",
 *
 *     }},"statusCode":true
 *     }
 *   }
 *     ), @SWG\Response(
 *         response = 422,
 *         description = "No Record Found.",
 *         examples={
 *     "application/json": {
 *     "errors":{
 *     {
 *       "field"="message",
 *       "message"="No Record Found"
 *     }
 *     },"statusCode":false
 *   }
 *     }
 *     ),
 * )
 */
