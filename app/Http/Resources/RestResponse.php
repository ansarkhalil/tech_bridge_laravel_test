<?php


namespace App\Http\Resources;


class RestResponse
{
    public $data;
    public $statusCode = true;
    public $httpCode = 200;

    /**
     * Display Success Response.
     *
     * @return Json
     */
    function success($data = null)
    {
        $arr['data'] = isset($data) ? $data : $this->data;
        $arr['status'] = $this->statusCode;
        return response()->json($arr, $this->httpCode);
    }

    /**
     * Display Success Response.
     *
     * @return Json
     */
    function failure($data = null)
    {
        $arr['errors']['message'] = (getenv('APP_DEBUG')) ? ( isset($data) ? $data : $this->data ) : 'Something Went Wrong';
        $arr['status'] = false;
        $this->httpCode = 500;
        return response()->json($arr, $this->httpCode);
    }

    /**
     * Display Success Response.
     *
     * @return Json
     */
    function validationFailed($data )
    {
        $arr['errors']['message'] = isset($data) ? $data : $this->data ;
        $arr['status'] = false;
        $this->httpCode = 422;
        return response()->json($arr, $this->httpCode);
    }
}