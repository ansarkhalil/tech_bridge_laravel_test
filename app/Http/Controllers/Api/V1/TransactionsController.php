<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Resources\RestResponse;
use App\Http\Resources\TransactionsResource;
use App\Transactions;
use App\Http\Resources\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator as Validator;
use Rap2hpoutre\FastExcel\FastExcel;


class TransactionsController extends Controller
{

    public $fileName;
    public $transactionsModel;
    public $response;

    /**
     * TransactionsController constructor
     */
    public function __construct()
    {
        $this->fileName = public_path().'/transactions.csv';
        $this->transactionsModel = new Transactions($this->fileName);
        $this->response = new RestResponse();
    }

    /**
     * Display a listing of the resource.
     *
     * @return TransactionsResource
     */
    public function index()
    {
        try {
            $collection = $this->transactionsModel->showAll();
            $this->response->data = $collection;

            return $this->response->success();

        } catch (\Exception $ex) {
            return $this->response->failure($ex->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'start_date' => 'required|date',
            'end_date' => 'required|date',
            'transaction_date' => 'required|date',
            'first_name' => 'required|max:50',
            'last_name' => 'required|max:50',
            'email' => 'required|email',
            'telnumber' => 'required|integer|min:11:max:11',
            'address1' => 'required|string',
            'city' => 'required|string',
            'country' => 'required|string',
            'product_name' => 'required|string',
            'postcode' => 'required|integer',
            'currency' => 'required|string',
            'cost' => 'required|numeric',
        ]);
        if ($validator->fails()) {
            return $this->response->validationFailed($validator->errors()->getMessages());
        }

        try {
            $collection = $this->transactionsModel->showAll();

            $filtered_collection = $collection->firstWhere('telnumber', $request['telnumber']);
            $this->response->data = $filtered_collection;


            if ($this->response->data) {
                return $this->response->validationFailed(['telnumber' => 'Already Exists in data set.']);
            }

            $collection->prepend($request->all());

            $this->transactionsModel->save($collection);

            return $this->response->success(['message' => 'Row Added Successfully']);


        } catch (\Exception $ex) {
            return $this->response->failure($ex->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $telnumber
     * @return RestResponse
     */
    public function show($telnumber)
    {
        if (!is_numeric($telnumber)) {
            return $this->response->validationFailed(['telnumber' => "Must be Integer"]);
        }
        try {
            $collection = $this->transactionsModel->showAll();

            $filtered_collection = $collection->firstWhere('telnumber', $telnumber);
            $this->response->data = $filtered_collection;

            if ($this->response->data) {
                return $this->response->success();
            } else {
                return $this->response->validationFailed(['telnumber' => "Doesn't Exists."]);
            }

        } catch (\Exception $ex) {
            return $this->response->failure($ex->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @return RestResponse
     */
    public function search(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'field' => 'required|max:20',
            'value' => 'required|max:50',
        ]);
        if ($validator->fails()) {
            return $this->response->validationFailed($validator->errors()->getMessages());
        }

        try {
            $collection = $this->transactionsModel->showAll();

            if (!isset($collection[0]) || !array_key_exists(trim($request['field']), $collection[0])) {
                return $this->response->validationFailed([$request['field'] => "Key " . $request['field'] . " Doesn't Exists in data set. "]);
            }

            $filtered_collection = $collection->where(trim($request['field']), trim($request['value']));
            $this->response->data = $filtered_collection->all();
            $collection_f = collect($this->response->data);

            if ($this->response->data) {
                (new FastExcel($collection_f))->export('invoices.csv');
                return $this->response->success();
            } else {
                return $this->response->validationFailed([$request['field'] => "No Record found against " . $request['field'] . " field "]);
            }

        } catch (\Exception $ex) {
            return $this->response->failure($ex->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return RestResponse
     */
    public function update(Request $request, $telnumber)
    {
        $validator = Validator::make($request->all(), [
            'start_date' => 'required|date',
            'end_date' => 'required|date',
            'transaction_date' => 'required|date',
            'first_name' => 'required|max:50',
            'last_name' => 'required|max:50',
            'email' => 'required|email',
            'address1' => 'required|string',
            'city' => 'required|string',
            'country' => 'required|string',
            'product_name' => 'required|string',
            'postcode' => 'required|integer',
            'currency' => 'required|string',
            'cost' => 'required|numeric',
        ]);
        if ($validator->fails()) {
            return $this->response->validationFailed($validator->errors()->getMessages());
        }

        try {
            $collection = $this->transactionsModel->showAll();

            $filtered_collection = $collection->search(function ($user) use ($telnumber) {
                return $user['telnumber'] === $telnumber;
            });

            if (!is_null($filtered_collection)) {
                $arr = [
                    'start_date' => $request['start_date'],
                    'end_date' => $request['end_date'],
                    'first_name' => $request['first_name'],
                    'last_name' => $request['last_name'],
                    'email' => $request['email'],
                    'telnumber' => $telnumber,
                    'address1' => $request['address1'],
                    'Address2' => $request['Address2'],
                    'city' => $request['city'],
                    'country' => $request['country'],
                    'postcode' => $request['postcode'],
                    'product_name' => $request['product_name'],
                    'cost' => $request['cost'],
                    'currency' => $request['currency'],
                    'transaction_date' => $request['transaction_date'],

                ];
                $collection[$filtered_collection] = $arr;
                $this->response->data = $filtered_collection;
            } else {
                return $this->response->validationFailed(['telnumber' => "Doesn't Exists in data set."]);
            }

            $this->transactionsModel->save($collection);

            return $this->response->success(['message' => 'Row Updated Successfully']);

        } catch (\Exception $ex) {
            return $this->response->failure($ex->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return RestResponse
     */
    public function destroy($telnumber)
    {
        if (!is_numeric($telnumber)) {
            return $this->response->validationFailed(['telnumber' => "Must be Integer"]);
        }
        try {
            $collection = $this->transactionsModel->showAll();

            $filtered_collection = $collection->search(function ($user) use ($telnumber) {
                return $user['telnumber'] === $telnumber;
            });
            if (!is_null($filtered_collection) && $filtered_collection !== false) {
                unset($collection[$filtered_collection]);
                $this->response->data = $filtered_collection;
            } else {
                return $this->response->validationFailed(['telnumber' => "Doesn't Exists in data set."]);
            }

            $this->transactionsModel->save($collection);

            return $this->response->success(['message' => 'Row Deleted Successfully']);

        } catch (\Exception $ex) {
            return $this->response->failure($ex->getMessage());
        }
    }
}
