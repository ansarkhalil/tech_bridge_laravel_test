<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class TransactionTest extends TestCase
{
    /**
     * All record fetch test .
     *
     * @return void
     */
    public function testIndex()
    {
        $response = $this->json('GET', '/api/v1/transactions');
        $response->assertStatus(200)
            ->assertJson([
                'data' => true
            ]);
    }

    /**
     * All record fetch by telnumber test .
     *
     * @return void
     */
    public function testShow()
    {
        $response = $this->json('GET', '/api/v1/transactions/34964830901');
        $response->assertStatus(200)
            ->assertJson([
                'data' => true
            ]);
    }

    /**
     * Search record  by any field test .
     *
     * @return void
     */
    public function testSearch()
    {
        $response = $this->json('GET', '/api/v1/transactions-search/',['field'=>'telnumber','value'=>'34964830901']);
        $response->assertStatus(200)
            ->assertJson([
                'data' => true
            ]);
    }

    /**
     * Delete Record  by Telnumber test .
     *
     * @return void
     */
    public function testDelete()
    {
        $response = $this->json('DELETE', '/api/v1/transactions/34964830901');
        $response->assertStatus(200)
            ->assertJson([
                'data' => true
            ]);
    }
}
